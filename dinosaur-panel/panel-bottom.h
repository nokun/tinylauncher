#ifndef PANELBOTTOM_H
#define PANELBOTTOM_H

#include <QWidget>
#include "launcher.h"

namespace Ui {
class PanelBottom;
}

class PanelBottom : public QWidget
{
    Q_OBJECT

public:
    explicit PanelBottom(QWidget *parent = nullptr, Launcher *launcher = nullptr);
    ~PanelBottom();

private:
    Launcher *launcher;
    Ui::PanelBottom *ui;

private slots:
    void btnMultitasking();
    void btnHome();
    void btnKeyboard();

};

#endif // PANELBOTTOM_H
